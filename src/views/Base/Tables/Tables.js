import React, { forwardRef, Component } from 'react'

import {withRouter} from 'react-router-dom'

import AddBox from '@material-ui/icons/AddBox'
import ArrowUpward from '@material-ui/icons/ArrowUpward'
import Check from '@material-ui/icons/Check'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import ChevronRight from '@material-ui/icons/ChevronRight'
import Clear from '@material-ui/icons/Clear'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import Edit from '@material-ui/icons/Edit'
import FilterList from '@material-ui/icons/FilterList'
import FirstPage from '@material-ui/icons/FirstPage'
import LastPage from '@material-ui/icons/LastPage'
import Remove from '@material-ui/icons/Remove'
import SaveAlt from '@material-ui/icons/SaveAlt'
import Search from '@material-ui/icons/Search'
import ViewColumn from '@material-ui/icons/ViewColumn'
// import MUIDataTable from 'mui-datatables'
// import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
// import Button from '@material-ui/core/Button';
// import { Check } from '@material-ui/icons'
import MaterialTable from 'material-table'

import axios from 'axios'
import StaticVar from '../../../config/StaticVar.js'

const server_url = StaticVar.SERVER_URL

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
}

withRouter(props => <Tables {...props}/>)

class Tables extends Component {
  constructor(props) {
    super(props)
    // console.log('this.props.location.pathname cons', this.props.location.pathname) 
    switch (this.props.location.pathname) {
    case '/setupdata/agama':
      this.columns = require('./Column/AgamaColumn.json')
      this.title = 'Agama'
      break   
    case '/setupdata/kelompok':
      this.columns = require('./Column/KelompokColumn.json')
      this.title = 'Kelompok'
      break 
    case '/setupdata/cabang':
      this.columns = require('./Column/CabangColumn.json')
      this.title = 'Cabang'
      break    
    
    case '/setupdata/globalvariable':
      this.columns = require('./Column/GlobalVariableColumn.json')
      this.title = 'Global Variable'
      break 
    case '/setupdata/screensession':
      this.columns = require('./Column/ScreenSessionColumn.json')
      this.title = 'Screen Session'
      break 
    default:
      this.columns = require('./Column/AgamaColumn.json')
      this.title = 'Agama'
      break
    }
    this.state = {  
      data: [ 
      ]
    }
  }
  
  componentDidMount() {
    console.log('this.props.location.pathname', this.props.location.pathname.replace(/setupdata\//gi, ''))
    // const { pathname } = this.props.location 
    this.getData()
  }
  
  getData() {
    const endpoint = `/ref${this.props.location.pathname.replace(/setupdata\//gi, '')}`
    console.log('endpoint', `${server_url}${endpoint}`)
    
    axios.get(`${server_url}${endpoint}`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
    ).then(result => {      
      console.log('data tertarik ', result)      
      this.setState({
        data: result.data
      })
    }).catch(e => {
      console.log('error', e)
    })
  }

  async crud(action, param) {
    const endpoint = `/ref${this.props.location.pathname.replace(/setupdata\//gi, '')}/`
    let result = null 
    try {
      switch (action.toLowerCase()) {
      case 'post':
        result = await axios.post(`${server_url}${endpoint}`, param
        )
        break
      case 'put':
        result = await axios.put(`${server_url}${endpoint}/${param.id}`, param
        )
        break
      case 'delete':
        result = await axios.delete(`${server_url}${endpoint}/${param.id}`)
        break
      default:
        break
      }
    } catch(e) {
      return e
    }
    return result
  }  

  
  render() {
    // const title = this.props.location.pathname
    return (
      <MaterialTable
        icons={tableIcons}
        // actions={[
        //   {
        //     icon: 'save',
        //     tooltip: 'Save User',
        //     onClick: (event, rowData) => {
        //       // Do save operation
        //     }
        //   }
        // ]}
        title={
          // `Master ${title.substring(1).charAt(0).toUpperCase() + title.substring(1).slice(1)}`
          this.title
        }
        columns={this.columns}
        data={this.state.data}
        editable={{
          onRowAdd: newData => 
            new Promise(resolve => {
              setTimeout(() => {
                resolve()
                this.crud('post', newData).then((e) => {
                  console.log('end post', e)                  
                  const data = [...this.state.data]
                  data.push(newData)
                  this.setState({ ...this.state, data })
                })
              }, 600)
            })
          ,
          onRowUpdate: (newData, oldData, resolve, reject) =>
            new Promise(resolve => {
              setTimeout(() => {
                console.log('oldData', oldData, resolve, reject)
                resolve()
                this.crud('put', newData).then((e) => {
                  console.log('end put', e)                  
                  const data = [...this.state.data]
                  data[data.indexOf(oldData)] = newData
                  this.setState({ ...this.state, data })
                })
              }, 600)
            }),
          onRowDelete: oldData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve()
                this.crud('delete', oldData).then((e) => {
                  console.log('end delete', e)                  
                  const data = [...this.state.data]
                  data.splice(data.indexOf(oldData), 1)
                  this.setState({ ...this.state, data })
                })
              }, 600)
            }),
        }}
      />
    )
  }
}
  
export default Tables
/*
  ["Name", "Company", "City", "State", {
    name: "Action",
    options: {
      filter: false,
      customBodyRender: (value, tableMeta, updateValue) => (
        <Button 
        color="primary" 
        onClick={() => {
          alert(tableMeta)
          console.log(tableMeta)
        }} 
        variant="contained">
        Proses
        </Button>
        )
      }
    }]
    
    //   <Row>
    //     <Col xs="12" lg="6">
    //       <Card>
    //         <CardHeader>
    //           <i className="fa fa-align-justify"></i> Simple Table
    //         </CardHeader>
    //         <CardBody>
    //           <Table responsive>
    //             <thead>
    //             <tr>
    //               <th>Username</th>
    //               <th>Date registered</th>
    //               <th>Role</th>
    //               <th>Status</th>
    //             </tr>
    //             </thead>
    //             <tbody>
    //             <tr>
    //               <td>Samppa Nori</td>
    //               <td>2012/01/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Estavan Lykos</td>
    //               <td>2012/02/01</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="danger">Banned</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Chetan Mohamed</td>
    //               <td>2012/02/01</td>
    //               <td>Admin</td>
    //               <td>
    //                 <Badge color="secondary">Inactive</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Derick Maximinus</td>
    //               <td>2012/03/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="warning">Pending</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Friderik Dávid</td>
    //               <td>2012/01/21</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             </tbody>
    //           </Table>
    //           <Pagination>
    //             <PaginationItem>
    //               <PaginationLink previous tag="button"></PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem active>
    //               <PaginationLink tag="button">1</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem>
    //               <PaginationLink tag="button">2</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem>
    //               <PaginationLink tag="button">3</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem>
    //               <PaginationLink tag="button">4</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem>
    //               <PaginationLink next tag="button"></PaginationLink>
    //             </PaginationItem>
    //           </Pagination>
    //         </CardBody>
    //       </Card>
    //     </Col>
    
    //     <Col xs="12" lg="6">
    //       <Card>
    //         <CardHeader>
    //           <i className="fa fa-align-justify"></i> Striped Table
    //         </CardHeader>
    //         <CardBody>
    //           <Table responsive striped>
    //             <thead>
    //             <tr>
    //               <th>Username</th>
    //               <th>Date registered</th>
    //               <th>Role</th>
    //               <th>Status</th>
    //             </tr>
    //             </thead>
    //             <tbody>
    //             <tr>
    //               <td>Yiorgos Avraamu</td>
    //               <td>2012/01/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Avram Tarasios</td>
    //               <td>2012/02/01</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="danger">Banned</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Quintin Ed</td>
    //               <td>2012/02/01</td>
    //               <td>Admin</td>
    //               <td>
    //                 <Badge color="secondary">Inactive</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Enéas Kwadwo</td>
    //               <td>2012/03/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="warning">Pending</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Agapetus Tadeáš</td>
    //               <td>2012/01/21</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             </tbody>
    //           </Table>
    //           <Pagination>
    //             <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
    //             <PaginationItem active>
    //               <PaginationLink tag="button">1</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
    //           </Pagination>
    //         </CardBody>
    //       </Card>
    //     </Col>
    //   </Row>
    
    //   <Row>
    
    //     <Col xs="12" lg="6">
    //       <Card>
    //         <CardHeader>
    //           <i className="fa fa-align-justify"></i> Condensed Table
    //         </CardHeader>
    //         <CardBody>
    //           <Table responsive size="sm">
    //             <thead>
    //             <tr>
    //               <th>Username</th>
    //               <th>Date registered</th>
    //               <th>Role</th>
    //               <th>Status</th>
    //             </tr>
    //             </thead>
    //             <tbody>
    //             <tr>
    //               <td>Carwyn Fachtna</td>
    //               <td>2012/01/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Nehemiah Tatius</td>
    //               <td>2012/02/01</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="danger">Banned</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Ebbe Gemariah</td>
    //               <td>2012/02/01</td>
    //               <td>Admin</td>
    //               <td>
    //                 <Badge color="secondary">Inactive</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Eustorgios Amulius</td>
    //               <td>2012/03/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="warning">Pending</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Leopold Gáspár</td>
    //               <td>2012/01/21</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             </tbody>
    //           </Table>
    //           <Pagination>
    //             <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
    //             <PaginationItem active>
    //               <PaginationLink tag="button">1</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
    //           </Pagination>
    //         </CardBody>
    //       </Card>
    //     </Col>
    
    //     <Col xs="12" lg="6">
    //       <Card>
    //         <CardHeader>
    //           <i className="fa fa-align-justify"></i> Bordered Table
    //         </CardHeader>
    //         <CardBody>
    //           <Table responsive bordered>
    //             <thead>
    //             <tr>
    //               <th>Username</th>
    //               <th>Date registered</th>
    //               <th>Role</th>
    //               <th>Status</th>
    //             </tr>
    //             </thead>
    //             <tbody>
    //             <tr>
    //               <td>Pompeius René</td>
    //               <td>2012/01/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Paĉjo Jadon</td>
    //               <td>2012/02/01</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="danger">Banned</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Micheal Mercurius</td>
    //               <td>2012/02/01</td>
    //               <td>Admin</td>
    //               <td>
    //                 <Badge color="secondary">Inactive</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Ganesha Dubhghall</td>
    //               <td>2012/03/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="warning">Pending</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Hiroto Šimun</td>
    //               <td>2012/01/21</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             </tbody>
    //           </Table>
    //           <Pagination>
    //             <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
    //             <PaginationItem active>
    //               <PaginationLink tag="button">1</PaginationLink>
    //             </PaginationItem>
    //             <PaginationItem className="page-item"><PaginationLink tag="button">2</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
    //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
    //           </Pagination>
    //         </CardBody>
    //       </Card>
    //     </Col>
    
    //   </Row>
    
    //   <Row>
    //     <Col>
    //       <Card>
    //         <CardHeader>
    //           <i className="fa fa-align-justify"></i> Combined All Table
    //         </CardHeader>
    //         <CardBody>
    //           <Table hover bordered striped responsive size="sm">
    //             <thead>
    //             <tr>
    //               <th>Username</th>
    //               <th>Date registered</th>
    //               <th>Role</th>
    //               <th>Status</th>
    //             </tr>
    //             </thead>
    //             <tbody>
    //             <tr>
    //               <td>Vishnu Serghei</td>
    //               <td>2012/01/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Zbyněk Phoibos</td>
    //               <td>2012/02/01</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="danger">Banned</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Einar Randall</td>
    //               <td>2012/02/01</td>
    //               <td>Admin</td>
    //               <td>
    //                 <Badge color="secondary">Inactive</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Félix Troels</td>
    //               <td>2012/03/01</td>
    //               <td>Member</td>
    //               <td>
    //                 <Badge color="warning">Pending</Badge>
    //               </td>
    //             </tr>
    //             <tr>
    //               <td>Aulus Agmundr</td>
    //               <td>2012/01/21</td>
    //               <td>Staff</td>
    //               <td>
    //                 <Badge color="success">Active</Badge>
    //               </td>
    //             </tr>
    //             </tbody>
    //           </Table>
    //           <nav>
    //             <Pagination>
    //               <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
    //               <PaginationItem active>
    //                 <PaginationLink tag="button">1</PaginationLink>
    //               </PaginationItem>
    //               <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
    //               <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
    //               <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
    //               <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
    //             </Pagination>
    //           </nav>
    //         </CardBody>
    //       </Card>
    //     </Col>
    //   </Row>
    */