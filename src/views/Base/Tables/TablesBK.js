import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
// import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
// import Button from '@material-ui/core/Button';
import { Check } from '@material-ui/icons'
import MaterialTable from 'material-table';

import axios from 'axios'

// const datatableData = [
//   ["Joe James", "Example Inc.", "Yonkers", "NY", "BTN"],
//   ["John Walsh", "Example Inc.", "Hartford", "CT", "BTN"],
//   ["Bob Herm", "Example Inc.", "Tampa", "FL", "BTN"],
//   ["James Houston", "Example Inc.", "Dallas", "TX", "BTN"],
//   ["Prabhakar Linwood", "Example Inc.", "Hartford", "CT", "BTN"],
//   ["Kaui Ignace", "Example Inc.", "Yonkers", "NY", "BTN"],
//   ["Esperanza Susanne", "Example Inc.", "Hartford", "CT", "BTN"],
//   ["Christian Birgitte", "Example Inc.", "Tampa", "FL", "BTN"],
//   ["Meral Elias", "Example Inc.", "Hartford", "CT", "BTN"],
//   ["Deep Pau", "Example Inc.", "Yonkers", "NY", "BTN"],
//   ["Sebastiana Hani", "Example Inc.", "Dallas", "TX", "BTN"],
//   ["Marciano Oihana", "Example Inc.", "Yonkers", "NY", "BTN"],
//   ["Brigid Ankur", "Example Inc.", "Dallas", "TX", "BTN"],
//   ["Anna Siranush", "Example Inc.", "Yonkers", "NY", "BTN"],
//   ["Avram Sylva", "Example Inc.", "Hartford", "CT", "BTN"],
//   ["Serafima Babatunde", "Example Inc.", "Tampa", "FL", "BTN"],
//   ["Gaston Festus", "Example Inc.", "Tampa", "FL", "BTN"],
//  ];


const columns = [
  {
    name: "id",
    label: 'ID',
    options:{
      filter: false,
      display: 'false'
    }
  },
  {
    name: "isactive",
    label: "Active",
    options: {
      filter: true,
      sort: true,
      customBodyRender: (value, tableMeta, updateValue) => (
        <Check />
        )
      }
    },
    {
      name: "created", 
      label: 'Created',
      options:{
        filter: false,
        display: 'false'
      }
    },
    {
      name: "createdby", 
      label: 'Created By',
      options:{
        filter: false,
        display: 'false'
      }
    },
    {
      name: "updated", 
      label: 'Updated',
      options:{
        filter: false,
        display: 'false'
      }
    },
    {
      name: "updateby", 
      label: 'Updated By',
      options:{
        filter: false,
        display: 'false'
      }
    },
    {
      name: "kode", 
      label: 'Kode',
    },
    {
      name: "nama", 
      label: 'Nama',
    },
    {
      name: "deskripsi", 
      label: "Deskripsi", 
    }
  ]
  
  const server_url = 'http://192.168.10.5:8080';
  class Tables extends Component {
    constructor(props) {
      super(props)
      this.state = { 
        data: []
      }
      this.chart = null
    }
    
    componentDidMount() {
      this.getData()
    }
    
    getData() {
      const endpoint = '/ref/agama'
      axios.get(`${server_url}${endpoint}`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
      ).then(result => {      
        console.log('data tertarik ', result)      
        this.setState({
          data: result.data
        })
      }).catch(e => {
        console.log('error', e)
      })
    }
    
    render() {
      return (
        <div className="animated fadeIn">
        <MUIDataTable
        title={"Data List"}
        data={this.state.data}
        columns={columns}
        options={{
          filterType: 'checkbox',
        }}
        />
        </div>
        )
        
      }
    }
    
    export default Tables;
    /*
    ["Name", "Company", "City", "State", {
      name: "Action",
      options: {
        filter: false,
        customBodyRender: (value, tableMeta, updateValue) => (
          <Button 
          color="primary" 
          onClick={() => {
            alert(tableMeta)
            console.log(tableMeta)
          }} 
          variant="contained">
          Proses
          </Button>
          )
        }
      }]
      
      //   <Row>
      //     <Col xs="12" lg="6">
      //       <Card>
      //         <CardHeader>
      //           <i className="fa fa-align-justify"></i> Simple Table
      //         </CardHeader>
      //         <CardBody>
      //           <Table responsive>
      //             <thead>
      //             <tr>
      //               <th>Username</th>
      //               <th>Date registered</th>
      //               <th>Role</th>
      //               <th>Status</th>
      //             </tr>
      //             </thead>
      //             <tbody>
      //             <tr>
      //               <td>Samppa Nori</td>
      //               <td>2012/01/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Estavan Lykos</td>
      //               <td>2012/02/01</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="danger">Banned</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Chetan Mohamed</td>
      //               <td>2012/02/01</td>
      //               <td>Admin</td>
      //               <td>
      //                 <Badge color="secondary">Inactive</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Derick Maximinus</td>
      //               <td>2012/03/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="warning">Pending</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Friderik Dávid</td>
      //               <td>2012/01/21</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             </tbody>
      //           </Table>
      //           <Pagination>
      //             <PaginationItem>
      //               <PaginationLink previous tag="button"></PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem active>
      //               <PaginationLink tag="button">1</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem>
      //               <PaginationLink tag="button">2</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem>
      //               <PaginationLink tag="button">3</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem>
      //               <PaginationLink tag="button">4</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem>
      //               <PaginationLink next tag="button"></PaginationLink>
      //             </PaginationItem>
      //           </Pagination>
      //         </CardBody>
      //       </Card>
      //     </Col>
      
      //     <Col xs="12" lg="6">
      //       <Card>
      //         <CardHeader>
      //           <i className="fa fa-align-justify"></i> Striped Table
      //         </CardHeader>
      //         <CardBody>
      //           <Table responsive striped>
      //             <thead>
      //             <tr>
      //               <th>Username</th>
      //               <th>Date registered</th>
      //               <th>Role</th>
      //               <th>Status</th>
      //             </tr>
      //             </thead>
      //             <tbody>
      //             <tr>
      //               <td>Yiorgos Avraamu</td>
      //               <td>2012/01/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Avram Tarasios</td>
      //               <td>2012/02/01</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="danger">Banned</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Quintin Ed</td>
      //               <td>2012/02/01</td>
      //               <td>Admin</td>
      //               <td>
      //                 <Badge color="secondary">Inactive</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Enéas Kwadwo</td>
      //               <td>2012/03/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="warning">Pending</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Agapetus Tadeáš</td>
      //               <td>2012/01/21</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             </tbody>
      //           </Table>
      //           <Pagination>
      //             <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
      //             <PaginationItem active>
      //               <PaginationLink tag="button">1</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
      //           </Pagination>
      //         </CardBody>
      //       </Card>
      //     </Col>
      //   </Row>
      
      //   <Row>
      
      //     <Col xs="12" lg="6">
      //       <Card>
      //         <CardHeader>
      //           <i className="fa fa-align-justify"></i> Condensed Table
      //         </CardHeader>
      //         <CardBody>
      //           <Table responsive size="sm">
      //             <thead>
      //             <tr>
      //               <th>Username</th>
      //               <th>Date registered</th>
      //               <th>Role</th>
      //               <th>Status</th>
      //             </tr>
      //             </thead>
      //             <tbody>
      //             <tr>
      //               <td>Carwyn Fachtna</td>
      //               <td>2012/01/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Nehemiah Tatius</td>
      //               <td>2012/02/01</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="danger">Banned</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Ebbe Gemariah</td>
      //               <td>2012/02/01</td>
      //               <td>Admin</td>
      //               <td>
      //                 <Badge color="secondary">Inactive</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Eustorgios Amulius</td>
      //               <td>2012/03/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="warning">Pending</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Leopold Gáspár</td>
      //               <td>2012/01/21</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             </tbody>
      //           </Table>
      //           <Pagination>
      //             <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
      //             <PaginationItem active>
      //               <PaginationLink tag="button">1</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
      //           </Pagination>
      //         </CardBody>
      //       </Card>
      //     </Col>
      
      //     <Col xs="12" lg="6">
      //       <Card>
      //         <CardHeader>
      //           <i className="fa fa-align-justify"></i> Bordered Table
      //         </CardHeader>
      //         <CardBody>
      //           <Table responsive bordered>
      //             <thead>
      //             <tr>
      //               <th>Username</th>
      //               <th>Date registered</th>
      //               <th>Role</th>
      //               <th>Status</th>
      //             </tr>
      //             </thead>
      //             <tbody>
      //             <tr>
      //               <td>Pompeius René</td>
      //               <td>2012/01/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Paĉjo Jadon</td>
      //               <td>2012/02/01</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="danger">Banned</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Micheal Mercurius</td>
      //               <td>2012/02/01</td>
      //               <td>Admin</td>
      //               <td>
      //                 <Badge color="secondary">Inactive</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Ganesha Dubhghall</td>
      //               <td>2012/03/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="warning">Pending</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Hiroto Šimun</td>
      //               <td>2012/01/21</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             </tbody>
      //           </Table>
      //           <Pagination>
      //             <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
      //             <PaginationItem active>
      //               <PaginationLink tag="button">1</PaginationLink>
      //             </PaginationItem>
      //             <PaginationItem className="page-item"><PaginationLink tag="button">2</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
      //             <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
      //           </Pagination>
      //         </CardBody>
      //       </Card>
      //     </Col>
      
      //   </Row>
      
      //   <Row>
      //     <Col>
      //       <Card>
      //         <CardHeader>
      //           <i className="fa fa-align-justify"></i> Combined All Table
      //         </CardHeader>
      //         <CardBody>
      //           <Table hover bordered striped responsive size="sm">
      //             <thead>
      //             <tr>
      //               <th>Username</th>
      //               <th>Date registered</th>
      //               <th>Role</th>
      //               <th>Status</th>
      //             </tr>
      //             </thead>
      //             <tbody>
      //             <tr>
      //               <td>Vishnu Serghei</td>
      //               <td>2012/01/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Zbyněk Phoibos</td>
      //               <td>2012/02/01</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="danger">Banned</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Einar Randall</td>
      //               <td>2012/02/01</td>
      //               <td>Admin</td>
      //               <td>
      //                 <Badge color="secondary">Inactive</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Félix Troels</td>
      //               <td>2012/03/01</td>
      //               <td>Member</td>
      //               <td>
      //                 <Badge color="warning">Pending</Badge>
      //               </td>
      //             </tr>
      //             <tr>
      //               <td>Aulus Agmundr</td>
      //               <td>2012/01/21</td>
      //               <td>Staff</td>
      //               <td>
      //                 <Badge color="success">Active</Badge>
      //               </td>
      //             </tr>
      //             </tbody>
      //           </Table>
      //           <nav>
      //             <Pagination>
      //               <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
      //               <PaginationItem active>
      //                 <PaginationLink tag="button">1</PaginationLink>
      //               </PaginationItem>
      //               <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
      //               <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
      //               <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
      //               <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
      //             </Pagination>
      //           </nav>
      //         </CardBody>
      //       </Card>
      //     </Col>
      //   </Row>
      */