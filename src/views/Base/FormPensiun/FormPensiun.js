import React, { Component } from 'react'
import {
  Button,
  Card,
  CardBody, 
  CardHeader,
  Col, 
  Form,
  FormGroup, 
  // FormFooter,
  Label,
  Row,
} from 'reactstrap'
import _ from 'lodash'
import CardMedia from '@material-ui/core/CardMedia'

import Select from 'react-select'


import moment from 'moment'

import axios from 'axios' 
import StaticVar from '../../../config/StaticVar'
import SystemVariable from '../../../lib/SystemVariable'


const server_url = StaticVar.SERVER_URL
  
class FormPensiun extends Component {  
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.toggleFade = this.toggleFade.bind(this)
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      data: {},
      dataterkait: '',
      listdataterkait: [],
      selectedOption: { value: null, label: '...' },
      // pesertaterpilih: null,
      options: [],
      nilaipensiun: 0
    }
  } 
  componentDidMount() {
    console.log('this.props.location.pathname', this.props.location.pathname)
    SystemVariable(this).get(server_url, this.props.location.pathname).then(() => {
      this.getDataPesertaDropdown()
    })
  }
  getDataPesertaDropdown() {
    const endpoint = '/master/peserta/dropdowndata'
    axios.get(`${server_url}${endpoint}`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
    ).then(result => {      
      console.log('data tertarik ', result)      
      this.setState({
        options: [{ value: null, label: '...' }, ...result.data]
      })
    }).catch(e => {
      console.log('error', e)
    })
  }  
  getData() {
    if (!this.state.selectedOption.value) {
      return
    }
    const endpoint = `/master/peserta/${this.state.selectedOption.value}`
    axios.get(`${server_url}${endpoint}`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
    ).then(result => {      
      console.log('data tertarik ', result)      
      this.setState({
        data: result.data
      })
    }).catch(e => {
      console.log('error', e)
    })
  }
  getInfo(action, idpeserta) {
    let api = ''
    switch(action)  {
    case 'skep':

      break
    default:
      api = 'keluarga'
      break
    }
    const endpoint = `/master/${api}`
    axios.get(`${server_url}${endpoint}`,
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
        }
      }
    ).then(result => {      
      console.log('data tertarik ', result)      
      const filteredData = _.filter(result.data, function(item){
        return item.peserta.id === idpeserta
      })
      this.setState({
        listdataterkait:  filteredData
      })
    }).catch(e => {
      console.log('error', e)
    })
  }
  toggle() {
    this.setState({ collapse: !this.state.collapse })
  }
  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }})
  }
  dataTerkaitAction(val) {
    this.setState({
      dataterkait: this.state.dataterkait === val ? '' : val
    },() => this.getInfo(this.state.dataterkait, this.state.data.id))
  }
  handleChange = selectedOption => {
    this.setState({ selectedOption }, () => this.getData())
    console.log(`Option selected: ${JSON.stringify(selectedOption)}`)
  }
  render() { 
    const profile = this.state.data
    const agama = this.state.data.agama ? this.state.data.agama : null
    const kelompok = this.state.data.kelompok ? this.state.data.kelompok : null
    const cabang = this.state.data.cabang ? this.state.data.cabang : null
    
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="15" md="12">
            <Card 
            // className={{maxWidth: 200}}
            > 
              <CardHeader>
                <strong>Pilih</strong> Notas
              </CardHeader>
              <CardBody style={{ padding: 10 }}>
                <Select
                  value={this.state.selectedOption}
                  onChange={this.handleChange}
                  options={this.state.options}
                />
                <Button color="primary" onClick={() => console.log(this.state)
                }>
                  tes
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row style={{ display: (this.state.selectedOption.value !== null) ? undefined : 'none'}}>
          {/* <Col xs="15" md="2">
            <Row style={{ flex: 1 }}>
              <Col>
                <Card 
                // className={{maxWidth: 200}}
                > 
                  <CardHeader>
                    <strong>Foto</strong> Profile
                  </CardHeader>
                  <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="310"
                    src={`${server_url}/arsip/foto/${profile.src_image}`}
                    title="Contemplative Reptile"
                  />                     
                </Card>
              </Col>
            </Row>
            
            <Row style={{ flex: 1 }}>
              <Col>
                <Card>
                  <CardHeader>
                    <strong>Informasi</strong> Terkait
                  </CardHeader>
                  <CardBody style={{ padding: 10 }}>
                    <Button onClick={() => this.dataTerkaitAction('informasikeluarga')} style={{ width: '100%', height: 53 }} color={this.state.dataterkait === 'informasikeluarga' ? undefined : 'primary'}>
                      Informasi Keluarga
                    </Button>
                  </CardBody>
                  <CardBody style={{ padding: 10 }}>
                    <Button onClick={() => this.dataTerkaitAction('historyskep')} variant="outlined" style={{ width: '100%', height: 53 }} color={this.state.dataterkait === 'historyskep' ? undefined : 'primary'}>
                      History SKEP
                    </Button>
                  </CardBody>
                  <CardBody style={{ padding: 10 }}>
                    <Button onClick={() => this.dataTerkaitAction('historycabang')} variant="outlined" style={{ width: '100%', height: 53 }} color={this.state.dataterkait === 'historycabang' ? undefined : 'primary'}>
                      History Cabang
                    </Button>
                  </CardBody>
                </Card>
              </Col>
            </Row>  
          </Col> */}

          <Col xs="15" md="6">
            <Card>
              <CardHeader>
                <strong>Data</strong> Pribadi
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="6" md="7">
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                      <FormGroup row>
                        <Col md="3">
                          <Label>Notas</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{profile.notas}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Nama</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{profile.nama}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Jenis Kelamin</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{profile.jenis_kelamin === 'L' ? 'Laki Laki' : 'Perempuan'}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Tanggal Lahir</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{moment(profile.tanggal_lahir, moment.defaultFormat).format('DD-MMMM-YYYY')}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Agama</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{agama ? agama.nama : ''}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Kelompok Peserta</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{kelompok ? kelompok.nama : ''}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Cabang</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{ cabang ? cabang.nama : ''}</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Batas Usia Pensiun</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{ profile.bup }</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Gaji Pokok</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{ (profile.gapok+'').replace(/\B(?=(\d{3})+(?!\d))/g, '.') }</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Jumlah Istri</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{ profile.jumlah_istri }</p>
                        </Col>
                      </FormGroup>
                      <FormGroup row>
                        <Col md="3">
                          <Label htmlFor="text-input">Jumlah Anak</Label>
                        </Col>
                        <Col xs="9" md="9">
                          <p className="form-control-static">{ profile.jumlah_anak }</p>
                        </Col>
                      </FormGroup>

                      {(this.state.nilaipensiun > 0) ?
                        <FormGroup row>
                          <Col md="3">
                            <Label htmlFor="text-input" style={{ fontWeight: 'bold' }}>Nilai Pensiun</Label>
                          </Col>
                          <Col xs="9" md="9">
                            <p style={{ fontWeight: 'bold' }} className="form-control-static">{(this.state.nilaipensiun+'').replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
                          </Col>
                        </FormGroup> : null
                      }

                      <Button onClick={() => {
                        console.log(SystemVariable(this).getGlobalVar('c'))
                        
                        this.setState({
                          nilaipensiun: SystemVariable(this).getGlobalVar('c') * profile.gapok
                        })
                      }} style={{ width: '100%', height: 53 }} color={'primary'}>
                        Hitung Pensiun
                      </Button>

                    </Form>
                  </Col>
                  <Col xs="6" md="4">
                    <Card 
                      // className={{maxWidth: 200}}
                    > 
                      {
                        (this.state.selectedOption.value) ?
                          <CardMedia
                            component="img"
                            alt="Contemplative Reptile"
                            height="290"
                            src={`${server_url}/arsip/foto/${profile.src_image}`}
                            title="Contemplative Reptile"
                          /> : null
                      }
                    </Card>
                  </Col>
                </Row>
              </CardBody> 
            </Card>
          </Col> 

        </Row>
        
      </div>
    )
  }
}

export default FormPensiun
