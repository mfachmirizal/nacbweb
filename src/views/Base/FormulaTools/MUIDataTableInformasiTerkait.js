import React from 'react'
import MUIDataTable from 'mui-datatables'
import moment from 'moment'
let columns = ['Name', 'Company', 'City', 'State']

let options = {
  filterType: 'checkbox',
}



class MUIDataTableInformasiTerkait extends React.Component {

  render() {
    const { title, data } = this.props
    let convertedTitle = title === 'informasikeluarga' ? 'Informasi Keluarga' : title === 'historyskep' ? 'History SKEP' : 'History Cabang'
    switch (title) {
    case 'informasikeluarga':
      convertedTitle = 'Informasi Keluarga'
      columns = [
        {
          name: 'id',
          label: 'ID',
          options: {
            filter: false,
            sort: false,
            display: false,
          }
        },
        {
          name: 'hubunganKeluarga',
          label: 'Hubungan Keluarga',
          options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              let result = ''
              if (value.charAt(0) === 'A') {
                result = 'Anak ke ' + (value.length > 1 ? value.charAt(1) : '' )
              }
              if (value.charAt(0) === 'I') {
                result = 'Istri ' + (value.length > 1 ? value.charAt(1) : '' )
              }
              return (
                <p style={{ marginBottom: 0 }} className="form-control-static">{result}</p>
              )
            }
          }
        },
        {
          name: 'jenis_kelamin',
          label: 'Jenis Kelamin',
          options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <p style={{ marginBottom: 0 }} className="form-control-static">{value === 'L' ? 'Laki Laki' : 'Perempuan'}</p>
              )
            }
          }
        },
        {
          name: 'nama',
          label: 'Nama',
          options: {
            filter: true,
            sort: false,
          }
        },
        {
          name: 'tanggal_lahir',
          label: 'Tanggal Lahir',
          options: {
            filter: true,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                moment(value, moment.defaultFormat).format('DD-MMMM-YYYY')
              )
            }
          }
        }
      ]
      break
    case 'historyskep':
      convertedTitle = 'History SKEP'
      break
    default:
      convertedTitle = 'History Cabang'
      break
    }
    return (
      <MUIDataTable
        title={convertedTitle}
        data={data}
        columns={columns}
        options={options}
      />)
  }
}
export default MUIDataTableInformasiTerkait
