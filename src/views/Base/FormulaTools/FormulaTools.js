import React, { Component } from 'react'
import {
  Button,
  Card,
  CardBody, 
  CardHeader,
  Col, 
  Form,
  FormGroup, 
  Input,
  Label,
  Row,
} from 'reactstrap'
import _ from 'lodash'
import CardMedia from '@material-ui/core/CardMedia'

import Select from 'react-select'


import moment from 'moment'

import axios from 'axios'
// import MUIDataTableInformasiTerkait from './MUIDataTableInformasiTerkait'
import StaticVar from '../../../config/StaticVar'
import SystemVariable from '../../../lib/SystemVariable';


const server_url = StaticVar.SERVER_URL
 
// const options = [
//   { value: null, label: '...' }, 
// ]

class FormPensiun extends Component {
  
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.toggleFade = this.toggleFade.bind(this)
    this.state = { 
      textarea: '',
      result: 0
    }
  }
 

  componentDidMount() {
    SystemVariable(this).get(server_url, this.props.location.pathname) 
  }
 

  toggle() {
    this.setState({ collapse: !this.state.collapse })
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }})
  }

  dataTerkaitAction(val) {
    this.setState({
      dataterkait: this.state.dataterkait === val ? '' : val
    },() => this.getInfo(this.state.dataterkait, this.state.data.id))
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption }, () => this.getData())
    console.log(`Option selected: ${JSON.stringify(selectedOption)}`)
  }

  render() {  
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="15" md="6">
            <Card>
              <CardHeader>
                <strong>Formula</strong> Tools
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input"> </Label>
                    </Col>
                    <Col xs="12" md="12">
                      <Input onChange={(val) => this.setState({ textarea: val.target.value }) } type="textarea" name="textarea-input" id="textarea-input" rows="9"
                        placeholder="Content..." />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Hasil</Label>
                    </Col>
                    <Col xs="9" md="9">
                      <p className="form-control-static">{ (this.state.result+'').replace(/\B(?=(\d{3})+(?!\d))/g, '.') }</p>
                    </Col>
                  </FormGroup>

                  <Button onClick={() => {
                    console.log('statenya', SystemVariable(this).evalValue(this.state.textarea))
                    const result = SystemVariable(this).evalValue(this.state.textarea)
                    this.setState({
                      result
                    })
                  }} style={{ width: '100%', height: 53 }} variant="contained" color="primary" >
                        Eksekusi
                  </Button>
                      
                </Form>
              </CardBody> 
            </Card>
          </Col> 
        </Row>
        
      </div>
    )
  }
}

export default FormPensiun
