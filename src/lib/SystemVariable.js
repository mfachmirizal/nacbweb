import axios from 'axios'

export default (context) => {
  return {
    get: async function (server_url, screen) {
      const modifScreen = screen.charAt(0) === '/' ? screen.substring(1) : screen
      console.log('get sistem',server_url, screen, modifScreen)            
          
      axios.get(`${server_url}/function/sistemvariable/${modifScreen}`,
        {
          headers: {
            'Access-Control-Allow-Origin': '*',
          }
        }
      ).then(result => {      
        // console.log('variable tertarik ', result)      
        context.setState({
          systemvar: result.data
        })
      }).catch(e => {
        console.log('error', e)
      }) 
    },
    getGlobalVar: function (name) {         
      return eval(context.state.systemvar.global[name])
    },   
    evalValue: function (value) {         
      let currVal = value
      const arrMatching = value.match(/@(.+?)@/g)
      // let populatedValue = 0
      console.log(arrMatching)
      
      if (arrMatching){
        for (const match of arrMatching) {
          const extractedValue = match.replace(/@/g, '')
          // console.log(context.state.systemvar.global[extractedValue])        
          // populatedValue = eval(context.state.systemvar.global[extractedValue]) + Number(populatedValue)
          // const regex = `/@${extractedValue}@/`
          // const re = new RegExp(regex,'g')
          
          const stateValue = context.state.systemvar.global[extractedValue]
          if (stateValue) {
            currVal = currVal.replace(match, `(${stateValue})`)
          } else {
            alert(`${match}, is not defined`)
            return 0
          }
        }
      }
      console.log('tes', currVal)
      // let result 
      try {
        // result = eval(currVal)
        return eval(currVal)
      } catch(e) {
        console.log(e)          
        alert(e)
        return 0
      }    
    }
    // getLocalVar: function (fromstate, name) {
    //   const targetVal = fromstate[name]         
    //   return eval(context.state.systemvar.global[name])
    // },   
  }
}